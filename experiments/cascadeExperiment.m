function detector = cascadeExperiment()
    detectorFile = 'cascade-out.xml';
    rerun = input('Retrain? (y or n) ', 's') == 'y';
    
    numObjectsPerImg = 1;
    
    if rerun
        delete(detectorFile);
        posImgDir = '../starcraft-images/positives/';
        negImgDir = '../starcraft-images/negatives/';
        posFiles = getAllFiles(posImgDir);
        negFiles = getAllFiles(negImgDir);

        numPosFiles = size(posFiles, 1);
        numNegFiles = size(negFiles, 1);
        display(strcat('# of positive files: ', num2str(numPosFiles)));
        display(strcat('# of negative files: ', num2str(numNegFiles)));

        numObjectsPerImg = 1;

        positives = struct('imageFilename', posFiles, 'objectBoundingBoxes', ones(numObjectsPerImg, 4));
        % Set each bounding box to be [1 1 imgWidth imgHeight]
        % Each bounding box is in the format, [x y width height]
        for i = 1:numPosFiles
            img = imread(posFiles{i});
            positives(i).objectBoundingBoxes(1,3) = size(img,2);
            positives(i).objectBoundingBoxes(1,4) = size(img,1);
        end

        import vision.*;
        % The false alarm rate is the fraction of negative training samples 
        %     incorrectly classified as positive samples.
        trainCascadeObjectDetector(detectorFile, positives, negFiles,...
            'ObjectTrainingSize', [32 32],...
            'NegativeSamplesFactor', 4,...
            'NumCascadeStages', 14,...
            'FalseAlarmRate', 0.50,...
            'TruePositiveRate', 0.90,...
            'FeatureType', 'HOG');
    end
        
    detector = vision.CascadeObjectDetector('cascade-out.xml');
        
    % A "test" image
    img = imread('../starcraft-images/test/Marine/mmm.jpg');
    bbox = step(detector, img);
    detectedImg = insertObjectAnnotation(img, 'rectangle', bbox, 'Marine');
    figure;
    imshow(detectedImg);
end
