function centristTester()
    %svm = centristExperiment();
    SVM_FILE = 'svm-centrist-hist.mat';
    SVM_TRAIN_FEATURES = 'svm-centrist-hist-train.mat'
    
    img = imread('../starcraft-images/test/Marine/mmm.jpg');
  
    display('Getting sliding windows');
    [windows, locations] = getSlidingWindows(img);
    numWindows = size(windows,4) * size(windows,5);
    
    tempImg = zeros(100, 100, 3);
    hog_4x4 = centrist(padImg(tempImg));  
    hogFeatureSize = length(hog_4x4);
    
    display('Assembling window hog features');
    featureNum = 1;
    
    hogFeatures  = zeros( numWindows, hogFeatureSize, 'single');
    for i = 1:size(windows,4)
        for j = 1:size(windows, 5)
            window = padImg( windows(:,:,:,i, j) );

            hog = centrist(window);
            hogFeatures( double(featureNum), :) = hog;
            featureNum = featureNum + 1;
        end
    end
    
    display('Classifying');
    
    load(SVM_FILE, 'model');
    load(SVM_TRAIN_FEATURES, 'trainingFeatures');
    
    tic
    testKernel = hist_isect( hogFeatures, trainingFeatures );
    toc
    save('testKernel.mat', 'testKernel');
    %load('testKernel.mat', 'testKernel');
    
    [labels, accuracy, decisions] = predict(ones(size(testKernel , 1), 1) .* 2, sparse(testKernel), model)
    %labels = svmclassify( svm, hogFeatures);
    % labels
    
    featureNum = 1;
    hits = 0;
    for i = 1:size(windows,4)
        for j = 1:size(windows, 5)
            if labels(featureNum) == 1
                figure,imshow(uint8(windows(:,:,:,i,j)))
                imwrite( uint8(windows(:,:,:,i,j)), strcat('../starcraft-images/gorune/', num2str(featureNum), '.png'))
                decisions(featureNum)
                hits = hits + 1;
                img = insertShape(img, 'Rectangle', [locations(featureNum, 2) locations(featureNum, 1) 48 48], 'Color', 'green');
            end
            featureNum = featureNum + 1;
        end
    end
    display(hits);
    figure, imshow(img);
    imwrite(img, 'last_result.png');
    % insertShape(img, 'Rectangle', X, 'Color', 'green')
    
    %detectedImg = insertObjectAnnotation(img, 'rectangle', bbox, 'Marine');
    %figure;
    %imshow(detectedImg);
end