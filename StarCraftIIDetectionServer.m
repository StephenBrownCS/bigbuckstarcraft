% StarCraftIIDetectionServer
%
% Runs the Detection component of the Detection Server
% It works closely with the StarCarftServer Java program
% to receive images, and return bounding boxes

% Here's how the program works:
% Repeat:
%   1) Wait for the presence of a query image
%   2) Check if query image is old, based on the time imprinted in its name
%      If it is, delete it and return to Step 1
%   3) Wait for image ready file to exist (this gets printed after the query 
%       image is present - otherwise, we might read it prematurely)
%   4) Read the Unit Query File. This tells us what units the AI wanted detections for
%   5) Run the requested detectors on the query image
%   6) Delete all the query files
%   7) Print the bounding boxes to a file
%   8) Create the "Response File Ready" File (This tells the Java server that the 
%      response file is ready to be read
%
function StarCraftIIDetectionServer()
    addpath('utilities');
    addpath('histogram-of-colors');
    
    QUERY_DIR = 'query-inbox/';
    if ~exist(QUERY_DIR, 'dir')
        mkdir(QUERY_DIR);
    end

    IMAGE_ARCHIVE = '../starcraft-images/server-archive/';

    commandCenterDetector = vision.CascadeObjectDetector( 'cascade-models/cascade-cc-out3.xml' );
    vespeneDetector = vision.CascadeObjectDetector( 'cascade-models/cascade-vespene-out-ajmaas4.xml' );
    barracksDetector = vision.CascadeObjectDetector( 'cascade-models/cascade-barracks-out.xml' );
    factoryDetector = vision.CascadeObjectDetector( 'cascade-models/cascade-factory-out.xml' );
    [mineralDetector, ~, mineralThreshold, ~] = trainHistogramOfColors();

    lastScreenShotKey = '2014_05_01_00_00_00';
    numCustomersServed = 0;
    sessionNum = 3;

    while true    
        % WAIT FOR QUERY IMAGE TO APPEAR
        while queryFilesExist(QUERY_DIR) == false
            display('Waiting for Query Image');
            pause(0.2);
        end
        
        tempFile = getLatestAvailableQueryFile(QUERY_DIR)
        screenShotKey = extractDateFromQueryFile(QUERY_DIR, tempFile);
        screenShotKey
        
        unitQueryFileName = strcat(QUERY_DIR, 'queryUnits-', screenShotKey, '.txt');
        queryImageFileName = strcat(QUERY_DIR, 'queryImg-', screenShotKey,'.png');
        imageReadyFileName = strcat(QUERY_DIR, 'queryImageReady-', screenShotKey, '.txt');
        
        [~, idx] = sort({screenShotKey, lastScreenShotKey});
        if idx(1) == 1
            display('Old Screenshot. Deleting ', screenShotKey)
            delete(unitQueryFileName);
            delete( queryImageFileName );
            delete( imageReadyFileName );
            continue;
        end
        
        % WAIT FOR QUERY IMAGE READY FILE
        while ~exist(imageReadyFileName, 'file')
            display('Waiting for Ready Signal');
            pause(0.1)
        end

        % READ IN WHAT UNITS WERE REQUESTED
        unitsOfInterest = [];
        unitsQueryFileId = fopen(unitQueryFileName, 'r');
        line = fgets(unitsQueryFileId);
        while ischar( line )
            unitsOfInterest = [unitsOfInterest ; cellstr(line)];
            line = fgets(unitsQueryFileId);
        end
        fclose(unitsQueryFileId);

        queryImg = imread(queryImageFileName);

        % RUN THE REQUESTED DETECTORS
        tic;
        commandCenterBoxes = runDetectorIfRequested(queryImg, unitsOfInterest, commandCenterDetector, 'buildings.CommandCenter');
        vespeneBoxes = runDetectorIfRequested(queryImg, unitsOfInterest, vespeneDetector, 'resources.Vespene');
        barracksBoxes = runDetectorIfRequested(queryImg, unitsOfInterest, barracksDetector, 'buildings.Barracks');
        factoryBoxes = runDetectorIfRequested(queryImg, unitsOfInterest, factoryDetector, 'buildings.Factory');
        
        mineralBoxes = [];
        if isempty(find(ismember(unitsOfInterest, 'resources.Mineral'))) == false || isempty(find(ismember(unitsOfInterest, 'All'))) == false
            BINS = 64;
            mineralBoxes = getHOCBoundingBoxes(queryImg, BINS, mineralDetector, mineralThreshold);
        end
        toc
        
        delete(unitQueryFileName);
        delete( queryImageFileName );
        delete( imageReadyFileName );

        % WRITE OUT THE BOUNDING BOXES FOR OUR NETWORK EMISSARY
        responseFileName = strcat(QUERY_DIR, 'response-', screenShotKey, '.txt');
        file = fopen(responseFileName, 'w');

        fprintf(file, strcat(num2str(size(commandCenterBoxes, 1) + size(vespeneBoxes, 1) + size(barracksBoxes, 1) + size(mineralBoxes, 1)), '\n') );

        printBoundingBoxesToFile(file, 'buildings.CommandCenter', commandCenterBoxes);
        printBoundingBoxesToFile(file, 'resources.Vespene', vespeneBoxes);
        printBoundingBoxesToFile(file, 'buildings.Barracks', barracksBoxes);
        printBoundingBoxesToFile(file, 'buildings.Factory', factoryBoxes);
        printBoundingBoxesToFile(file, 'resources.Mineral', mineralBoxes);
        
        fprintf(file, '\n');

        fclose(file);

        % WRITE RESPONSE FILE READY FILE
        responseReadyFileName = strcat(QUERY_DIR, 'responseReady-', screenShotKey, '.txt');
        doneIndicatorFile = fopen(responseReadyFileName, 'w');
        fclose(doneIndicatorFile);

        % This isn't really necessary, but good for debugging
        imwrite(queryImg, strcat(IMAGE_ARCHIVE, 'queryImg-', num2str(sessionNum), '-', num2str(numCustomersServed), '.png'));
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', commandCenterBoxes, 'Command Center');
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', vespeneBoxes, 'Vespene Geyser');
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', barracksBoxes, 'Barracks');
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', factoryBoxes, 'Factory');
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', mineralBoxes, 'Minerals');
        figure(1), imshow(queryImg);
        
        numCustomersServed = numCustomersServed + 1;
        lastScreenShotKey = screenShotKey;
    end
end

% Checks if the unit name is in the unitsOfInterest list (or if it contains
% "All")
% If so, runs the detector upon it and returns the bounding boxes
function bboxes = runDetectorIfRequested( queryImg, unitsOfInterest, detector, unitName )
    bboxes = [];
    
    if isempty(find(ismember(unitsOfInterest, unitName))) == false || isempty(find(ismember(unitsOfInterest, 'All'))) == false
        bboxes = step(detector, queryImg);
    end
end


% Iterates through the bounding boxes, and prints them to a file,
% comma-separating them.
function printBoundingBoxesToFile( file, unitName, bboxes )
    if (isempty(bboxes) == false)
        for i = 1 : size(bboxes, 1)
            fprintf(file, '%s %i %i %i %i,', unitName, bboxes(i, 1), bboxes(i, 2), bboxes(i, 3), bboxes(i, 4));
        end
    end
end


function filesAvailable = queryFilesExist(queryDir)
    latestFile = getLatestAvailableQueryFile(queryDir);
    filesAvailable = ~isempty(latestFile);
end


function latestFile = getLatestAvailableQueryFile(queryDir)
    fileNames = getAllFiles(queryDir);
    latestFile = [];
    for i = length(fileNames) : -1 : 1
        if isempty(findstr('queryImg', fileNames{i})) == false
            latestFile = fileNames{i};
            return
        end
    end
end


function screenShotKey = extractDateFromQueryFile(queryDir, str)
    formatStr = strcat(queryDir, 'queryImg-%19c');
    screenShotKey = sscanf( str, formatStr);
end
