
public enum ServerDetectionsManager {
    SINGLETON;

    private volatile String lastJobKeyLaunched;
    private volatile String lastJobKeyCompleted;

    private ServerDetectionsManager(){
        lastJobKeyLaunched = Util.getDateString();
        lastJobKeyCompleted = Util.getDateString();
    }
    
    public synchronized boolean testAndSetLastJobKeyLaunched( String jobKey ){
        if( lastJobKeyLaunched.compareTo( jobKey ) < 0){
            lastJobKeyLaunched = jobKey;
            return true;
        }
        return false;
    }

    public synchronized void registerJobCompletion( String jobKey ){
        if( lastJobKeyCompleted.compareTo( jobKey ) < 0){
            lastJobKeyCompleted = jobKey;
        }
    }
    
    
    public boolean isStaleJob( String jobKey ){
        return lastJobKeyCompleted.compareTo( jobKey ) > 0;
    }
}
