function detector = trainVespeneCascade()
    addpath('../utilities');

    detectorFile = '../cascade-models/cascade-vespene-out-ajmaas4.xml';
    rerun = input('Retrain? (y or n) ', 's') == 'y';
            
    myaddress = 'cs766vision@gmail.com';
    mypassword = 'moneyball123';
    setpref('Internet','E_mail',myaddress);
    setpref('Internet','SMTP_Server','smtp.gmail.com');
    setpref('Internet','SMTP_Username',myaddress);
    setpref('Internet','SMTP_Password',mypassword);

    props = java.lang.System.getProperties;
    props.setProperty('mail.smtp.auth','true');
    props.setProperty('mail.smtp.socketFactory.class', ...
        'javax.net.ssl.SSLSocketFactory');
    props.setProperty('mail.smtp.socketFactory.port','465');
    
    if rerun
        %delete(detectorFile);
        posImgDir = '../starcraft-images/new-vespene-positives/';
        negImgDir = '../starcraft-images/vespene-negatives/';
        posFiles = getAllFiles(posImgDir);
        negFiles = getAllFiles(negImgDir);

        numPosFiles = size(posFiles, 1);
        numNegFiles = size(negFiles, 1);
        display(strcat('# of positive files: ', num2str(numPosFiles)));
        display(strcat('# of negative files: ', num2str(numNegFiles)));

        numObjectsPerImg = 1;

        positives = struct('imageFilename', posFiles, 'objectBoundingBoxes', ones(numObjectsPerImg, 4));
        % Set each bounding box to be [1 1 imgWidth imgHeight]
        % Each bounding box is in the format, [x y width height]
        sumWidth = 0;
        sumHeight = 0;
        for i = 1:numPosFiles
            img = imread(posFiles{i});
            width = size(img,2);
            height = size(img,1);
            sumWidth = sumWidth + size(img,2);
            sumHeight = sumHeight + size(img,1);
            positives(i).objectBoundingBoxes(1,3) = size(img,2);
            positives(i).objectBoundingBoxes(1,4) = size(img,1);
        end
        avgWidth = sumWidth / numPosFiles
        avgHeight = sumHeight / numPosFiles
                                
        display('*** BEGINNING TRAINING ***');
        import vision.*;
        % The false alarm rate is the fraction of negative training samples 
        %     incorrectly classified as positive samples.
        trainCascadeObjectDetector(detectorFile, positives, negFiles,...
            'ObjectTrainingSize', [100 100],...
            'NegativeSamplesFactor', 2,...
            'NumCascadeStages', 12,...
            'FalseAlarmRate', 0.50,...
            'TruePositiveRate', 0.995,...
            'FeatureType', 'Hog');
    end
    
        
    detector = vision.CascadeObjectDetector( detectorFile );
    
    sendmail('StephenBrownCS@gmail.com', 'Vespene Training Is Done', strcat('The Alisa Joy Maas Mk. IV Vespene Geyser Cascade Object Detector is ready to be deployed.'));
    
    %testImages = getAllFiles('../starcraft-images/command-center-test/');
    %testImages = getAllFiles('../starcraft-images/new-vespene-originals/')
    
    %{
    testImages = getAllFiles('../starcraft-images/server-archive/')
    for i = 1 : length(testImages)
        % A "test" image
        %img = imread('../starcraft-images/test/Marine/sparse_units_many_marines.png');
        img = imread( testImages{i} );
        
        bbox = step(detector, img);
        detectedImg = insertObjectAnnotation(img, 'rectangle', bbox, 'Vespene');
        figure;
        imshow(detectedImg);
        
        %saveBoundingBoxes(img, bbox, i);
    end
    %}
end
