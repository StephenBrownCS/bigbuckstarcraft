function [megaHistogram, minScore, maxScore, avgScore] = trainHistogramOfColors(NUM_BINS)
    if ~exist('NUM_BINS','var')
        NUM_BINS = 64;
    end
    NUM_CHANNELS = 3;
   
    %% Make histrogram
    megaHistogram = ones(NUM_BINS, NUM_CHANNELS);
    trainingImages = getAllFiles('../starcraft-images/stephen-windows/mineral-positives/');
    for i = 1 : length(trainingImages)
        img = imread(trainingImages{i});
        megaHistogram = megaHistogram + computeHistogramOfColors(img, NUM_BINS);
    end
    megaHistogram = uint8(megaHistogram ./ length(trainingImages));  
    
    %% Get statistics
    minScore = 100000;
    maxScore = 0;
    avgScore = 0;
    
    for i = 1 : length(trainingImages)
       hist = computeHistogramOfColors(imread(trainingImages{i}),NUM_BINS);
       score = megaHistogram - uint8(hist);
       score = score .* score; 
       score = sum(sum(score));
       if minScore > score
            minScore = score;
       end
       if maxScore < score
           maxScore = score;
       end
       avgScore = avgScore + score;
    end
    avgScore = avgScore / length(trainingImages);  
end