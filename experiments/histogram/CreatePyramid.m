function [pyramidFeatures] = CreatePyramid( imgDir, dataDir, createDictionary )
    % A Wrapper around BuildPyramid which uses the parameters we want
    % It uses all images in imgDir and all subdirectories of imgDir

    fileNames = getAllFiles(imgDir);

    params.maxImageSize = 1000;
    params.gridSpacing = 8;
    params.patchSize = 16;
    params.dictionarySize = 2048;
    params.numTextonImages = 50;
    params.pyramidLevels = 3;
    params.oldSift = false;
    
    % return pyramid descriptors for all files in fileNames
    canSkip = 1;
    saveSift = 1;
    pyramidFeatures = BuildPyramid(fileNames,'.', dataDir,params, canSkip, saveSift, createDictionary);
end
