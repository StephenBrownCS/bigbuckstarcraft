function [windows, locations] = getSlidingWindows( img )
   NUM_CHANNELS = 3;
   WINDOW_SIZE = 48;
   WIDTH = size(img, 2);
   HEIGHT = size(img, 1);
   STEP_SIZE = WINDOW_SIZE /2;
   
   
   windowRangeH = 1:STEP_SIZE:HEIGHT - WINDOW_SIZE;
   numWindowRows = length(windowRangeH);
   windowRangeW = 1:STEP_SIZE:WIDTH - WINDOW_SIZE;
   numWindowCols = length(windowRangeW);
   windows = zeros( WINDOW_SIZE, WINDOW_SIZE, NUM_CHANNELS, numWindowRows, numWindowCols );
   size(windows)
   tic
   counter = 1;
   locations = zeros(numWindowRows * numWindowCols, 2);
   for i = 1 : numWindowRows
       for j = 1 : numWindowCols
           windowH = windowRangeH(i);
           windowW = windowRangeW(j);
           locations( counter, : ) = [windowH, windowW];
           windows(:,:,:, i, j) = img(windowH : windowH + WINDOW_SIZE - 1, windowW : windowW + WINDOW_SIZE - 1, :);   
           counter = counter + 1;
       end
   end
   toc
    display('done');
end