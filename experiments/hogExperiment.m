function svm = hogExperiment()
    posDirName = '../starcraft-images/south-positives/';
    negDirName = '../starcraft-images/negatives/';

    SVM_FILE = 'svm.mat';
    
    
    % get file names and number of images
    posFileNames = dir(strcat(posDirName, '*.png'));
    numPosImages = length(posFileNames);

    % read first image for the purposes of figuring out our dimensions
    % this returns a 3D color map - rows, cols for each RGB channel
    %tempImg = padImg(imread(strcat(posDirName, posFileNames(1).name),'png'));
    %imgHeight = size(tempImg,1);
    %imgWidth = size(tempImg,2);

    % read the rest of the images into the fourth dimension
    
    
    cellSize = [4 4];
    
    tempImg = zeros(100, 100, 3);
    [hog_4x4, ~] = extractHOGFeatures(padImg(tempImg),'CellSize',[4 4]);
    hogFeatureSize = length(hog_4x4);
   %{
    
    display('Computing Features');
    %posTrainingFeatures = assembleHogFeatureVector( posDirName, cellSize, hogFeatureSize);
    posTrainingFeatures = assembleHogFeatureVector( '../starcraft-images/positives', cellSize, hogFeatureSize);
    
    negTrainingFeatures = assembleHogFeatureVector( negDirName, cellSize, hogFeatureSize);
    
    %marauderNegatives = assembleHogFeatureVector( '../starcraft-images/negatives/Marauder/Idle/', cellSize, hogFeatureSize);
    
    trainingFeatures = [posTrainingFeatures; negTrainingFeatures];
    
    numPosInstances = size( posTrainingFeatures, 1);
    classLabels = [ones( numPosInstances,1); ones(size(negTrainingFeatures, 1),1) .* 2];
    
    display('training SVM');
    svm = svmtrain(trainingFeatures, classLabels);
    save(SVM_FILE, 'svm');
    %}
    
    load(SVM_FILE, 'svm');
    
   
    display('testing SVM');
    testFeatures = assembleHogFeatureVector( '../starcraft-images/Dirt Dark/', cellSize, hogFeatureSize);
    svmclassify( svm, testFeatures)
    
    %testFeatures = assembleHogFeatureVector( '../starcraft-images/positives/Marines/Grass Light/', cellSize, hogFeatureSize, 1:48 );
    %svmclassify( svm, testFeatures)
    
    %testFeatures =  assembleHogFeatureVector( '../starcraft-images/negatives/Marauder/Moving/', cellSize, hogFeatureSize );
    %svmclassify( svm, testFeatures);
    
    %testFeatures =  assembleHogFeatureVector( '../starcraft-images/test/Marine/mmmWindows', cellSize, hogFeatureSize );
    %labels = svmclassify( svm, testFeatures)
    %fileNames = getAllFiles( '../starcraft-images/test/Marine/mmmWindows' );
    %{
    for i = 1 : size(labels)
        if labels(i) == 1
            display(fileNames{i});
            figure;
            imshow(fileNames{i});
        end
    end
    %}
    
    display('done');
end

