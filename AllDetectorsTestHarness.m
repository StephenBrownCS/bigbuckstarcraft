% Test Harness for taking in a test image, and running all of our detectors upon it
function AllDetectorsTestHarness()
    addpath('utilities');
    addpath('histogram-of-colors');
    
    IMAGE_ARCHIVE = '../starcraft-images/server-archive/';

    commandCenterDetector = vision.CascadeObjectDetector( 'cascade-cc-out3.xml' );
    vespeneDetector = vision.CascadeObjectDetector( 'cascade-vespene-out-ajmaas4.xml' );
    barracksDetector = vision.CascadeObjectDetector( 'cascade-barracks-out.xml' );
    factoryDetector = vision.CascadeObjectDetector( 'cascade-factory-out.xml' );
    [mineralDetector, ~, mineralThreshold, ~] = trainHistogramOfColors();

    testImages = getAllFiles(IMAGE_ARCHIVE);
    for i = 1 : 30%length(testImages)
        % A "test" image
        %img = imread('../starcraft-images/test/Marine/sparse_units_many_marines.png');
        queryImg = imread( testImages{i} );

        % Acquire Bounding Box from detectors
        tic;
        commandCenterBoxes = runDetectorIfRequested(queryImg, commandCenterDetector, 'buildings.CommandCenter');
        vespeneBoxes = runDetectorIfRequested(queryImg, vespeneDetector, 'resources.Vespene');
        barracksBoxes = runDetectorIfRequested(queryImg, barracksDetector, 'buildings.Barracks');
        factoryBoxes = runDetectorIfRequested(queryImg, factoryDetector, 'buildings.Factory');
        
        BINS = 64;
        mineralBoxes = getHOCBoundingBoxes(queryImg, BINS, mineralDetector, mineralThreshold);
        toc

        % Write the bounding boxes out for the network emissary to scoop up
        %imwrite(queryImg, strcat(IMAGE_ARCHIVE, 'resultImg.png'));

        % This isn't really necessary, but good for debugging
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', commandCenterBoxes, 'Command Center');
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', vespeneBoxes, 'Vespene Geyser');
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', barracksBoxes, 'Barracks');
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', factoryBoxes, 'Factory');
        queryImg = insertObjectAnnotation(queryImg, 'rectangle', mineralBoxes, 'Minerals');
        figure;
        imshow(queryImg);
    end
end


function bboxes = runDetectorIfRequested( queryImg, detector, unitName )
    bboxes = step(detector, queryImg);
end
