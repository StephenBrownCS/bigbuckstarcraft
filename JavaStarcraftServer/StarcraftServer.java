import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * A multi-threaded server created to serve as the endpoint of communication on the host 
 * running the detectors. It sets up a ServerSocket and listens for connections 
 * from StarCraft AI clients. For each client, it spins off a new thread and 
 * has it handle the request. It does not maintain any long-lived connections. 
 * Every request is its own connection.
 * 
 * @author Stephen
 *
 */
public class StarcraftServer {
    private ServerSocket serverSocket;
    private ThreadPoolExecutor tpe;
    private final int LISTENING_PORT;


    /** Launches a server and tells it to run */
    public static void main(String[] args) {
        StarcraftServer server = new StarcraftServer( 8002 );
        server.runServer();
    }


    public StarcraftServer(int listeningPort){
        LISTENING_PORT = listeningPort;
        tpe = new ThreadPoolExecutor(5, 15, 15, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(5));  
    }


    /**
     * Opens up a server socket and has it listen for and handle connections.
     */
    public void runServer() {
        try{
            serverSocket = new ServerSocket(LISTENING_PORT);
            System.out.println("Listening at " + LISTENING_PORT);
            
            DetectionRequestListener matlabListener = new DetectionRequestListener();
            matlabListener.start();
        }
        catch(IOException e){
            System.out.println("IOException");
            System.out.println("It is likely that a port is already in use");
            System.exit(0);
        }
    }



    /** 
     * This class listens for incoming client connections and handles them 
     * with a separate thread.
     * @author Stephen
     */
    private class DetectionRequestListener extends Thread{
        public void run(){
            Thread listenerThread = Thread.currentThread();              

            //HANDLE REQUESTS UNTIL SHUT DOWN OR INTERRUPTION
            System.out.println("Awaiting connection");
            while(!listenerThread.isInterrupted()) {
                Socket socket = null;
                try{
                    socket = serverSocket.accept();
                    socket.setSoTimeout(5000);  

                    tpe.execute(new ServerDetectionRequestHandler(socket));
                }
                catch(RejectedExecutionException e){
                    System.out.println("FAILURE: Client Connection refused");
                    e.printStackTrace();
                    Util.closeSocket(socket);
                }
                catch(IOException e){
                    System.out.println("IOException while trying to accept socket");
                    e.printStackTrace();
                }
            }
        }
        
        public void interrupt(){
            super.interrupt();
            Util.closeServerSocket(serverSocket);
        }

        
    }
}
