import java.awt.Image;
import java.awt.image.*;
import java.io.*;
import java.net.*;

import javax.imageio.ImageIO;
import java.util.List;

public class Receiver1 {
    public static void main(String[] args) {
        try {
            int DUMMY_PORT = 8003;
            int PACKET_LENGTH = 1024;

            // Create a socket to listen on the port.
            DatagramSocket dsocket = new DatagramSocket(DUMMY_PORT);

            // Create a buffer to read datagrams into. If a
            // packet is larger than this buffer, the
            // excess will simply be discarded!
            byte[] buffer = new byte[PACKET_LENGTH+3];


            //System.out.println(buffer);

            // Create a packet to receive data into the buffer
            // DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

            //System.out.println(buffer);
            int offset =buffer.length;


            ByteArrayOutputStream inStream = new ByteArrayOutputStream();

            byte[] whole = new byte[195*PACKET_LENGTH];

            System.out.println(whole.length);

            System.out.println("Entering loop");

            // Now loop forever, waiting to receive packets and adding them to the memory stream
            while (true) {
                // Wait to receive a datagram
                DatagramPacket packet = new DatagramPacket(buffer, PACKET_LENGTH+3);
                packet.setData(buffer,0, PACKET_LENGTH+3);

                //inStream.write(buffer, 0, PACKET_LENGTH+3);

                offset+=PACKET_LENGTH+3;

                try{
                    System.out.println("Wating for packet...");
                    dsocket.receive(packet);
                    System.out.println("Received");
                }catch(IOException e){
                    System.out.println( "Server IOException " + e );

                }
                System.out.print(packet);

                byte[] inPacket = new byte[PACKET_LENGTH+3];

                packet.setData(inPacket);

                //Add the contents of buffer to the Output Stream
                for (int i=0;i<buffer.length;i++){
                    whole[i]=buffer[i];
                }
                System.out.println(packet.getLength());


                // Reset the length of the packet before reusing it.
                packet.setLength(buffer.length);
            }

            /*
            File f=new File("pic.jpg");



            OutputStream outStream =new FileOutputStream(f);

            outStream.write(buffer);



            ImageIO.write((RenderedImage) inStream, "jpg", f);
             */


        } catch (Exception e) {
            System.err.println(e);
        }

    }
}