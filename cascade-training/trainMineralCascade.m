%% DO NOT USE. HOC PERFORMS MUCH BETTER FOR MINERALS

function detector = trainMineralCascade()
    myaddress = 'cs766vision@gmail.com';
    mypassword = 'moneyball123';  
	setpref('Internet','E_mail',myaddress);
    setpref('Internet','SMTP_Server','smtp.gmail.com');
    setpref('Internet','SMTP_Username',myaddress);
    setpref('Internet','SMTP_Password',mypassword);

    props = java.lang.System.getProperties;
    props.setProperty('mail.smtp.auth','true');
    props.setProperty('mail.smtp.socketFactory.class', ...
        'javax.net.ssl.SSLSocketFactory');
    props.setProperty('mail.smtp.socketFactory.port','465');
    

    detectorFile = 'cascade-mineral-out-new-positives.xml';
    rerun = input('Retrain? (y or n) ', 's') == 'y';
        
    
    if rerun
        delete(detectorFile);
        posImgDir = '../starcraft-images/stephen-windows/mineral-positives/';
        negImgDir = '../starcraft-images/mineral-negatives/';
        posFiles = getAllFiles(posImgDir);
        negFiles = getAllFiles(negImgDir);

        numPosFiles = size(posFiles, 1);
        numNegFiles = size(negFiles, 1);
        display(strcat('# of positive files: ', num2str(numPosFiles)));
        display(strcat('# of negative files: ', num2str(numNegFiles)));

        numObjectsPerImg = 1;

        positives = struct('imageFilename', posFiles, 'objectBoundingBoxes', ones(numObjectsPerImg, 4));
        % Set each bounding box to be [1 1 imgWidth imgHeight]
        % Each bounding box is in the format, [x y width height]
        sumWidth = 0;
        sumHeight = 0;
        for i = 1:numPosFiles
            img = imread(posFiles{i});
            sumWidth = sumWidth + size(img,2);
            sumHeight = sumHeight + size(img,1);
            positives(i).objectBoundingBoxes(1,3) = size(img,2);
            positives(i).objectBoundingBoxes(1,4) = size(img,1);
        end
        avgWidth = sumWidth / numPosFiles
        avgHeight = sumHeight / numPosFiles
                        
        display('*** BEGINNING TRAINING ***');
        import vision.*;
        % The false alarm rate is the fraction of negative training samples 
        %     incorrectly classified as positive samples.
        trainCascadeObjectDetector(detectorFile, positives, negFiles,...
            'ObjectTrainingSize', [75 75],...
            'NegativeSamplesFactor',0.2,...
            'NumCascadeStages', 8,...
            'FalseAlarmRate', 0.50,...
            'TruePositiveRate', 0.995,...
            'FeatureType', 'HOG');
    end
    
        
    detector = vision.CascadeObjectDetector( detectorFile );
     
    sendmail('coreyaschulz@gmail.com', 'Mineral Training Is Done', strcat('Your Job is Done'));
    
    testImages = getAllFiles('../starcraft-images/agria-valley/agria-valley-test/');
    for i = 1 : length(testImages)
        % A "test" image
        %img = imread('../starcraft-images/test/Marine/sparse_units_many_marines.png');
        img = imread( testImages{i} );
        bbox = step(detector, img);
        saveBoundingBoxes(img, bbox, 100 + i, 'W:/u/s/c/schulzca/cs766/starcraft-images/mineral-negatives/');
        detectedImg = insertObjectAnnotation(img, 'rectangle', bbox, 'Minerals');
        figure;
        imshow(detectedImg);
    end
    
end
