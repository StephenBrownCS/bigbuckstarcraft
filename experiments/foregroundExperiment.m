detector = cascadeExperiment();


videoName = '../videos/CollosusVsMarines.mp4';
% implay( videoName );
video = VideoReader(videoName);
numFrames = video.NumberOfFrames
height = video.Height;
width = video.Width;
frameRate = video.FrameRate

% The rate to run our detector at
detectorRate = 5; % in frames per second

framesToSkip = floor(frameRate / detectorRate)

mov(1:numFrames) = struct('cdata',zeros(height,width, 3,'uint8'), 'colormap',[]);
       
display('READING VIDEO');
for k = 1 : numFrames
    mov(k).cdata = read(video,k);
end

display('GETTING BOUNDING BOXES');
%Single Threaded Version
 
foregroundDetector = vision.ForegroundDetector('NumGaussians', 3, 'NumTrainingFrames', 50);

% Set framesWaited to be high so that we compute during the first iteration
framesWaited = 100;
totalDelay = 0;
totalSteps = 0;
max = 0;
numFramesWithFGDetector = 0;
k = 2;
while k < numFrames
    tic
    if framesWaited > framesToSkip
        bbox = step(detector, mov(k).cdata);
        totalSteps = totalSteps + 1;
        framesWaited = 0;
    end
    
    mov(k).cdata = insertObjectAnnotation(mov(k).cdata, 'rectangle', bbox, 'Marine');
    
    
    tic
    difference = norm( reshape(double(mov(k).cdata(:,:,1) - mov(k - 1).cdata(:,:,1)), [480 640]));
    if difference > 6000 || numFramesWithFGDetector > 100
        foregroundDetector = vision.ForegroundDetector('NumGaussians', 3, 'NumTrainingFrames', 50);
        display('RESET');
        numFramesWithFGDetector = 0;
    end
    
    if difference > max 
        max = difference;
    end
    
    
    foreground = step(foregroundDetector, mov(k).cdata);
    %figure(1),imshow(foreground,[]);\
   
    fgParty = foreground;
    fgParty(:,:,2) = foreground;
    fgParty(:,:,3) = foreground;
        
    figure(2),imshow( uint8( double(mov(k).cdata) .* fgParty ), []);
    framesWaited = framesWaited + 1;
    delay = toc;
    
    skippy = delay * frameRate;
    k = floor(k + delay * frameRate);
    k = k + 1;
    
    numFramesWithFGDetector = numFramesWithFGDetector + 1;
end

totalDelay
avgDelay = totalDelay / totalSteps

