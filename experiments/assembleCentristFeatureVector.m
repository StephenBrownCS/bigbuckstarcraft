function [hogFeatures] = assembleCentristFeatureVector( dirName, hogFeatureSize, range )
    %dirName = strcat(dirName, '/');

    %fileNames = dir(strcat(dirName, '*.png'));
    fileNames = getAllFiles( dirName );
    if ~exist('range', 'var')
        range = 1:length(fileNames);
    end
    
    numImages = size(range,2);
    hogFeatures  = zeros( numImages, hogFeatureSize, 'single');
    for i = 1:numImages
        img = imread(fileNames{i});
        img = padImg(img);

        hog = centrist(img);
        hogFeatures(i,:) = hog;
    end
end

