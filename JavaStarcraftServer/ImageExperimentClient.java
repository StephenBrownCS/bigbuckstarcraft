import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.imageio.ImageIO;


public class ImageExperimentClient {
    public Robot robot;
    public double total;

    public static void main(String[] args) throws AWTException{
        ImageExperimentClient c = new ImageExperimentClient();        
        c.robot = new Robot();

        BufferedImage img = c.takeScreenShot();

        for(int i = 0; i < 1000; i++){
            c.run("Chuck-Dyer", img);
        }
        
        System.out.println("Avg Time: " + c.total / 10);
    }

    public void run(String screenShotKey, BufferedImage img){
        Socket socket = null;

        
        try{
            System.out.println(screenShotKey + ": " + "launching...");
            long startTime = System.nanoTime();

            socket = new Socket();
            socket.connect(new InetSocketAddress("localhost", 8003), 10000);
            socket.setSoTimeout(5000);  
            BufferedOutputStream writer = new BufferedOutputStream(socket.getOutputStream());
            BufferedReader br = new BufferedReader( new InputStreamReader(socket.getInputStream()));

            writer.write((screenShotKey + "\n").getBytes());
            writer.flush();

            Set<String> unitsOfInterest = new LinkedHashSet<String>();
            unitsOfInterest.add("Resources.KivolowitzKrystals");
            for( String unitName : unitsOfInterest ){
                writer.write((unitName + ",").getBytes());
            }
            writer.write("\n".getBytes());
            writer.flush();

            ImageIO.write(img, "JPG", (writer));

            double elapsedTime = (System.nanoTime() - startTime) / Math.pow(10, 9);
            total += elapsedTime;
            System.out.println(screenShotKey + ": elapsed time: " + elapsedTime );
        }
        catch(SocketTimeoutException e){
            System.out.println(screenShotKey + ": XXXXXXXX" + " timed out.");
        }
        catch(RequestCancelledException e){
            System.out.println(screenShotKey + ": " + " cancelled.");
        }
        catch(IOException e){
            System.out.println(screenShotKey + ": XXXXXXXX" + e.getMessage());
        }
        finally{
            System.out.println(screenShotKey + ": " +  " terminated");
            Util.closeSocket(socket);
        }
    }

    public BufferedImage takeScreenShot(){
        // TODO FOR COREY
        Rectangle rect = new Rectangle(0, 0, 1400, 900);
        BufferedImage img = robot.createScreenCapture(rect);
        return img;
    }
}
