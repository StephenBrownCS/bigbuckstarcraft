function [ bbox ] = mergeBoxes( bbox, overlapAmount )
    index = 1;
    while index <= size(bbox,1)
       overlap = bbox(index, :);
       others = bbox(1:index-1,:);
       for check = index + 1:size(bbox,1)
           if intersects(bbox(index,:),bbox(check,:))
              overlap = [overlap; bbox(check,:)]; 
           else
               if isempty(others)
                  others = bbox(check,:); 
               else
                  others = [others; bbox(check,:)];
               end
           end
       end
       
       if size(overlap,1) >= overlapAmount
           if isempty(others)
               others = mergeAll(overlap);
           else
               others = [mergeAll(overlap); others];
           end
           bbox = others;           
       end
     
       index = index + 1;
    end
end

function intersects = intersects(r1, r2)
    intersects = ~(r2(1,1) > r1(1,1)+r1(1,3) ...
                    || r2(1,1)+r2(1,3) < r1(1,1) ...
                    || r2(1,2) > r1(1,2)+r1(1,4) ...
                    || r2(1,2) + r2(1,4) < r1(1,2));
end

function merged = mergeAll(rects)
    x = rects(1,1);
    y = rects(1,2);
    right = rects(1,1) + rects(1,3);
    bottom = rects(1,2) + rects(1,4);
    for i = 2:size(rects,1)
       x = min(x, rects(i,1));
       y = min(y, rects(i,2));
       right = max(right, rects(i,1) + rects(i,3));
       bottom = max(bottom, rects(i,2) + rects(i,4));
    end
    
    merged = [x, y, right - x, bottom - y];
end