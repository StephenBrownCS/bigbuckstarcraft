function hogTester()
    svm = hogExperiment();
    
    img = imread('../starcraft-images/test/Marine/interrace_battle.jpg');
    
    %% SEGMENT IMAGE
    gray = rgb2gray(img);
    mask = zeros(size(gray));
    mask(100:end-400, 1:end) = 1;
    figure, imshow(img(120:end-450, 1:end, :));
    bw = activecontour(gray, mask, 10000);
    figure, imshow(bw);
    return;
    %%
    
    
    display('Getting sliding windows');
    [windows, locations] = getSlidingWindows(img);
    numWindows = size(windows,4) * size(windows,5);
    cellSize = [4 4];
    
    tempImg = zeros(100, 100, 3);
    [hog_4x4, ~] = extractHOGFeatures(padImg(tempImg),'CellSize',[4 4]);  
    hogFeatureSize = length(hog_4x4);
    
    display('Assembling window hog features');
    featureNum = 1;
    
    hogFeatures  = zeros( numWindows, hogFeatureSize, 'single');
    for i = 1:size(windows,4)
        for j = 1:size(windows, 5)
            window = padImg( windows(:,:,:,i, j) );
            %img = padImg(imread(strcat(dirName, fileNames(range(i)).name),'png'));

            %figure(1), imshow(img);
            %hold on;
            %[hog_2x2, vis2x2] = extractHOGFeatures(img,'CellSize',[2 2]);
            %[hog_4x4, vis4x4] = extractHOGFeatures(img,'CellSize',[4 4]);
            %[hog_8x8, vis8x8] = extractHOGFeatures(img,'CellSize',[8 8]);
            %plot(vis2x2);

            [hog, ~] = extractHOGFeatures(window,'CellSize',cellSize);
            hogFeatures( double(featureNum), :) = hog;
            featureNum = featureNum + 1;
        end
    end
    
    display('Classifying');
    labels = svmclassify( svm, hogFeatures);
    labels
    
    featureNum = 1;
    hits = 0;
    for i = 1:size(windows,4)
        for j = 1:size(windows, 5)
            if labels(featureNum) == 1
                %figure,imshow(uint8(windows(:,:,:,i,j)))
                hits = hits + 1;
                img = insertShape(img, 'Rectangle', [locations(featureNum, 2) locations(featureNum, 1) 48 48], 'Color', 'green');
            end
            featureNum = featureNum + 1;
        end
    end
    display(hits);
    figure, imshow(img);
    imwrite(img, 'last_result.png');
    % insertShape(img, 'Rectangle', X, 'Color', 'green')
    
    %detectedImg = insertObjectAnnotation(img, 'rectangle', bbox, 'Marine');
    %figure;
    %imshow(detectedImg);
end