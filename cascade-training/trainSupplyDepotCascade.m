function detector = trainSupplyDepotCascade()
    addpath('../utilities');

    detectorFile = '../cascade-models/cascade-supply-depot2.xml';
    rerun = input('Retrain? (y or n) ', 's') == 'y';
        
    setUpJobCompletionEmail();
    
    if rerun
        delete(detectorFile);
        %posImgDir = '../starcraft-images/stephen-windows/marine-east-positives/';
        posImgDir = '../starcraft-images/stephen-windows/supply-depot-positives/';
        negImgDir = '../starcraft-images/supply-depot-negatives/';
        posFiles = getAllFiles(posImgDir);
        negFiles = getAllFiles(negImgDir);

        numPosFiles = size(posFiles, 1);
        numNegFiles = size(negFiles, 1);
        display(strcat('# of positive files: ', num2str(numPosFiles)));
        display(strcat('# of negative files: ', num2str(numNegFiles)));

        numObjectsPerImg = 1;

        positives = struct('imageFilename', posFiles, 'objectBoundingBoxes', ones(numObjectsPerImg, 4));
        % Set each bounding box to be [1 1 imgWidth imgHeight]
        % Each bounding box is in the format, [x y width height]
        sumWidth = 0;
        sumHeight = 0;
        for i = 1:numPosFiles
            img = imread(posFiles{i});
            width = size(img,2);
            height = size(img,1);
            sumWidth = sumWidth + size(img,2);
            sumHeight = sumHeight + size(img,1);
            positives(i).objectBoundingBoxes(1,3) = size(img,2);
            positives(i).objectBoundingBoxes(1,4) = size(img,1);
        end
        avgWidth = sumWidth / numPosFiles
        avgHeight = sumHeight / numPosFiles
                                        
        display('*** BEGINNING TRAINING ***');
        import vision.*;
        % The false alarm rate is the fraction of negative training samples 
        %     incorrectly classified as positive samples.
        trainCascadeObjectDetector(detectorFile, positives, negFiles,...
            'ObjectTrainingSize', [100 100],...
            'NegativeSamplesFactor', 2,...
            'NumCascadeStages' , 15,...
            'FalseAlarmRate', 0.50,...
            'TruePositiveRate', 0.995,...
            'FeatureType', 'Hog');
    end
    
        
    detector = vision.CascadeObjectDetector( detectorFile );
    
    sendmail('StephenBrownCS@gmail.com', 'Sasha Seacow: Supply Depot Training Is Done', 'Your Job is Done');
    
    %testImages = getAllFiles('../starcraft-images/command-center-test/');
    %testImages = getAllFiles('../starcraft-images/new-vespene-originals/')
    
    
    %testImages = getAllFiles('../starcraft-images/test-marine/')
    testImages = getAllFiles('../starcraft-images/server-archive/session0/')
    for i = 1 : 20%length(testImages)
        % A "test" image
        %img = imread('../starcraft-images/test/Marine/sparse_units_many_marines.png');
        img = imread( testImages{i} );
        
        bbox = step(detector, img);
        detectedImg = insertObjectAnnotation(img, 'rectangle', bbox, 'Marine');
        figure;
        imshow(detectedImg);
        
        %saveBoundingBoxes(img, bbox, i);
    end
   
end
