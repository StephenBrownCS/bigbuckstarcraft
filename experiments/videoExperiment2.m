% In this experiment, I used a videoFileReader and VideoPlayer

detector = cascadeExperiment();

videoFReader = vision.VideoFileReader('../videos/CollosusVsMarines.mp4');
videoPlayer = vision.VideoPlayer;

%numFrames = videoFReader.NumberOfFrames
%height = videoFReader.Height;
%width = videoPlayer.Width;
%frameRate = videoPlayer.FrameRate

% The rate to run our detector at
framesToWait = 5; % in frames per second
framesWaited = framesToWait + 1;
%framesToSkip = floor(frameRate / detectorRate)

while ~isDone(videoFReader)
  videoFrame = step(videoFReader);
  if framesWaited > framesToWait
      tic
      bbox = step(detector, videoFrame);
      framesWaited = 0;
      toc
  end
  videoFrame = insertObjectAnnotation(videoFrame, 'rectangle', bbox, 'Marine');
  step(videoPlayer, videoFrame);
  framesWaited = framesWaited + 1;
end
release(videoPlayer);
release(videoFReader)