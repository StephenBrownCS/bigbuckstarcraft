function img = removeHUD( img )
    imgHeight = size(img, 1);
    newHeight = floor(imgHeight * 0.7);
    img = img( 1: newHeight, :, : );
end