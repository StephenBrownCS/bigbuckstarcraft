function padded = padImg( img )
    %img = rgb2gray(img);
    %[m, n] = size(img);
    %p = 100;
    %q = 100;
    %padded = padarray(img, [floor((p-m)/2) floor((q-n)/2)], 'replicate','post');
    %padded = padarray(padded, [ceil((p-m)/2) ceil((q-n)/2)], 'replicate','pre');
    
    %padded = imresize(img, [40, 40]);
    MAX_SIZE = 32;
    rows = min(MAX_SIZE, size(img, 1));
    cols = min(MAX_SIZE, size(img, 2));
    padded = zeros(MAX_SIZE, MAX_SIZE);
    padded(1:rows, 1:cols, 1) = img(1:rows,1:cols,1);
    padded(1:rows, 1:cols, 2) = img(1:rows,1:cols,2);
    padded(1:rows, 1:cols, 3) = img(1:rows,1:cols,3);
    
    padded = uint8(padded);
    %figure,imshow(padded);
end