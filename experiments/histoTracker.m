% This one is an order of magnitude faster
% It sets up a tracker and does that at each iteration instead of the 
% cascade detection. I figure we should run the cascade detector every 
% once in a while, and then track for most frames.
% Currently, it gets absolutely terrible performance, since I don't have 
% the updated training data, and I'm initializing it before marines
% are actually in the frame.

detector = cascadeExperiment();

videoFReader = vision.VideoFileReader('../videos/CollosusVsMarines.mp4');
videoInfo    = info(videoFReader);
videoPlayer = vision.VideoPlayer;

%numFrames = videoFReader.NumberOfFrames
%height = videoFReader.Height;
%width = videoPlayer.Width;
%frameRate = videoPlayer.FrameRate

% The rate to run our detector at
framesToWait = 5; % in frames per second
framesWaited = framesToWait + 1;
%framesToSkip = floor(frameRate / detectorRate)

videoFrame = step(videoFReader);
bbox = step(detector, videoFrame);

% ======== CREATE TRACKER. TO TRACK THINGS! =========
% Create a tracker object.
tracker = vision.HistogramBasedTracker;

% Use the hue channel to track
% Get the skin tone information by extracting the Hue from the video frame
% converted to the HSV color space.
[hueChannel,~,~] = rgb2hsv(videoFrame);

% Initialize the tracker histogram using the Hue channel pixels from the
% nose.
initializeObject(tracker, hueChannel, bbox(1,:));

while ~isDone(videoFReader)
  videoFrame = step(videoFReader);
  if framesWaited > framesToWait
      tic
      % RGB -> HSV
      [hueChannel,~,~] = rgb2hsv(videoFrame);

      % Track using the Hue channel data
      bbox = step(tracker, hueChannel);
      framesWaited = 0;
      toc
  end
  videoFrame = insertObjectAnnotation(videoFrame, 'rectangle', bbox, 'Marine');
  step(videoPlayer, videoFrame);
  framesWaited = framesWaited + 1;
end
release(videoPlayer);
release(videoFReader)