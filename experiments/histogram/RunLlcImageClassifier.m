function RunLlcImageClassifier(trainImgDir, testImgDir, dataDir, useHistogramKernel)
    % Creates spatial pyramid features using LLC for the coding scheme, and
    % max pooling with L2 normalization for the compile step. It first creates the
    % spatial pyramid for the training data and trains an SVM with those features.
    % It then creates the spatial pyramid for the test set, and asks the SVM
    % for predictions on them.
    % trainImgDir - directory full of training images (need not be flat)
    % testImgDir - directory full of testing images (need not be flat)
    % dataDir - directory for storing intermediate data such as SIFT features and 
    %           the dictionary of textons
    % useHistogramKernel - if 1, will use the histogram intersection kernel
    %                      if 0, will use a linear kernel

    addpath('liblinear-1.94/windows');
    addpath('liblinear-1.94/matlab'); % For Unix

    if useHistogramKernel
        display('Using Histogram Kernel');
    else
        display('Using Linear Kernel');
    end
    
    % TRAIN USING ALL IMAGES OF TEST SET (first 100 of given dataset)
    imageClasses = {'bedroom', ...
                  'CALsuburb', ...
                  'industrial', ...
                  'kitchen',...
                  'livingroom',...
                  'MITcoast',...
                  'MITforest',...
                  'MIThighway',...
                  'MITinsidecity',...
                  'MITmountain',...
                  'MITopencountry',...
                  'MITstreet',...
                  'MITtallbuilding',...
                  'PARoffice',...
                  'store'};
          
    %==========================================================================
    %                           TRAINING PHASE
    %==========================================================================
    display('***Assembling Pyramid of Training Instances***');
    % Each row of this will represent a training instance with 1000+ columns
    % where each column is a feature    
    pyramidFeatures = CreatePyramid(trainImgDir, dataDir, 1);

    display('***Training SVM***');
    trainFileNames = getAllFiles(trainImgDir);
    trainingLabels = createLabelVector(trainFileNames, imageClasses);   

    % Train directly on kernel matrix - it's like using each of the training 
    % instances as a new basis (new dimension space)
    if useHistogramKernel
        trainInstances = hist_isect(pyramidFeatures, pyramidFeatures);
    else
        trainInstances = linearKernel(pyramidFeatures, pyramidFeatures);
    end
    model = train(trainingLabels, sparse(trainInstances), '-c 100');

    %==========================================================================
    %                           TESTING PHASE
    %==========================================================================
    display('***Assembling Pyramid of Testing Instances***');
    % We use the same data dir, since we want to use the same dictionary
    pyramidTestFeatures = CreatePyramid(testImgDir, dataDir, 0);

    display('***Run SVM on Test Set - Getting Predictions***');
    testFileNames = getAllFiles(testImgDir);
    testingLabels = createLabelVector(testFileNames, imageClasses);

    % Take the kernel of each test instance with the training instances
    % It's like mapping the test instances into training dimension space
    if useHistogramKernel
        testKernel = hist_isect(pyramidTestFeatures, pyramidFeatures);
    else
        testKernel = linearKernel(pyramidTestFeatures, pyramidFeatures);
    end
    predictedLabels = predict(testingLabels, sparse(testKernel), model);
    
    %==========================================================================
    %                         ACCURACY METRICS
    %==========================================================================
    % Calculate low-level metrics
    correctClassCounts = zeros(15, 1);      % # of instances we get right of each class
    actualClassCounts = zeros(15,1);        % # of instances actually in each class
    for i = 1 : size(testingLabels, 1)
        actualLabel = testingLabels(i);
        predictedLabel = predictedLabels(i);
        if actualLabel == predictedLabel
            correctClassCounts(actualLabel) = correctClassCounts(actualLabel) + 1;
        end
        actualClassCounts(actualLabel) = actualClassCounts(actualLabel) + 1;
    end
    
    % Calculate mean of the accuracies
    sum = 0;
    for i = 1 : 15
        sum = sum + correctClassCounts(i) / actualClassCounts(i);
    end
    meanAccuracy = sum / 15
    
    confusionMatrix = confusionmat(testingLabels, predictedLabels)
end


function testingLabels = createLabelVector( fileNames, imageClasses )
    % Make a class label vector, which will have a class label in each row,
    % one for each instance (a file)
    % Label each image class as 1, 2, 3, (corresponds to index into imageClasses) 

    testingLabels = zeros(size(fileNames,1), 1);
    for i = 1 : size(fileNames, 1)
        for j = 1 : 15
            if isempty(findstr(imageClasses{j}, fileNames{i})) == false
                testingLabels(i) = j;
            end
        end
    end
end
