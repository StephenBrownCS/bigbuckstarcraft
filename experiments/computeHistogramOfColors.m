function histogram = computeHistogramOfColors(img, numBins)

        
    %img = imread('../starcraft-images/cc-area-mineral-positives/cc-area-mineral-positive-001.png')
    IMG_HEIGHT = size(img,1);
    IMG_WIDTH = size(img,2);
    NUM_CHANNELS = 3;
            
    [redCounts, ~] = imhist(img(:,:,1), numBins);
    [greenCounts, ~] = imhist(img(:,:,2), numBins);
    [blueCounts, ~] = imhist(img(:,:,3), numBins);

    normalizationThing = IMG_HEIGHT * IMG_WIDTH / 256;
    redCounts = redCounts / normalizationThing;
    greenCounts = greenCounts / normalizationThing;
    blueCounts = blueCounts / normalizationThing;
    
    %{
    display(img(1:10, 1:10, 1));
    img = uint8(img ./ 10);
    display(img(1:10, 1:10, 1));
    
    hist(img)
    
    return
    
    for r = 1 : IMG_HEIGHT
        for c = 1 : IMG_WIDTH
            
        end
    end
    %}
    
    histogram = [redCounts, greenCounts, blueCounts];
end

