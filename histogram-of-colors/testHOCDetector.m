BINS = 64;
[histogram, min, max, avg] = trainHistogramOfColors(BINS);

testImages = getAllFiles('../starcraft-images/agria-valley/agria-valley-test/nalxulu/');
for i = 1 : 10;
    % A "test" image
    %img = imread('../starcraft-images/test/Marine/sparse_units_many_marines.png');
    img = imread( testImages{i} );
    bbox = getHOCBoundingBoxes(img, BINS, histogram, max);
    detectedImg = insertObjectAnnotation(img, 'rectangle', bbox, 'Minerals');
    figure;
    imshow(detectedImg);
end