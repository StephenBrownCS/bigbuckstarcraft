
import os
import os.path as path
import sys, getopt
    
imageClasses = ['bedroom', 
              'CALsuburb', 
              'industrial', 
              'kitchen',
              'livingroom',
              'MITcoast',
              'MITforest',
              'MIThighway',
              'MITinsidecity',
              'MITmountain',
              'MITopencountry',
              'MITstreet',
              'MITtallbuilding',
              'PARoffice',
              'store'];

srcDir = 'testSet/'
trainDir = 'trainSet/'
testDir = 'testSet/'

if not os.path.exists(trainDir):
    os.mkdir(trainDir);
if not os.path.exists(testDir):
    os.mkdir(testDir);

for imgClass in imageClasses:
    print 'Copying over ' + imgClass
    srcPath = srcDir + imgClass + '/'
    trainPath = trainDir + imgClass + '/'
    
    if not os.path.exists(trainPath):
        os.mkdir(trainPath);
    testPath = testDir + imgClass + '/'
    
    if not os.path.exists(testPath):
        os.mkdir(testPath);
        
    os.system('mv ' + srcPath + 'image_00* ' + trainPath + '.')
    os.system('mv ' + srcPath + 'image_0100.jpg ' + trainPath + '.')
    os.system('mv ' + srcPath + '* ' + testPath + '.');

print '\n'

