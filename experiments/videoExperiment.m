detector = trainCCcascade();


videoName = '../starcraft-videos/TerranStart.mp4';
% implay( videoName );
video = VideoReader(videoName);
numFrames = video.NumberOfFrames
height = video.Height;
width = video.Width;
frameRate = video.FrameRate

% The rate to run our detector at
detectorRate = 5; % in frames per second

framesToSkip = floor(frameRate / detectorRate)

mov(1:numFrames) = struct('cdata',zeros(height,width, 3,'uint8'), 'colormap',[]);
       
display('READING VIDEO');
for k = 1 : numFrames
    mov(k).cdata = read(video,k);
end

display('GETTING BOUNDING BOXES');
%Single Threaded Version
 
% Set framesWaited to be high so that we compute during the first iteration
framesWaited = 100;
totalDelay = 0;
totalSteps = 0;
for k = 1 : numFrames
    
    if framesWaited > framesToSkip
        tic;
        bbox = step(detector, mov(k).cdata);
        delay = toc;
        totalDelay = totalDelay + delay;
        totalSteps = totalSteps + 1;
        framesWaited = 0;
    end
    
    mov(k).cdata = insertObjectAnnotation(mov(k).cdata, 'rectangle', bbox, 'Command Center');
    figure(1),imshow(mov(k).cdata,[]);
    framesWaited = framesWaited + 1;
    
    skippy = delay * frameRate
    k = k + delay * frameRate;
end

totalDelay
avgDelay = totalDelay / totalSteps

% Multi-threaded version
%{
parpool(2)
spmd(2)
    if labindex() == 1
        for k = 1 : 125
            figure(1),imshow(mov2(k).cdata,[]);
        end
    else
        for k = 1 : framesToSkip : 125
            bbox = step(detector, mov2(k).cdata);
            framesWaited = 0;
            blah = insertObjectAnnotation(mov2(k).cdata, 'rectangle', bbox, 'Marine');
            figure(2),imshow(blah,[]);
        end
    end
end
delete(gcp)
%}

%hf = figure;
%set(hf, 'position', [150 150 width height])
%movie(hf, mov, 1, video.FrameRate);

% videoFrames = read( video );
% firstFrame = videoFrames(:,:,:,1);
