function patternExperiment
  threshold = single(0.98);
  level = 2;

  hVideoSrc = vision.VideoFileReader('../starcraft-videos/CollosusVsMarines.mp4',...
                                      'VideoOutputDataType', 'single',...
                                      'ImageColorSpace', 'Intensity');
  hGaussPymd1 = vision.Pyramid('PyramidLevel', level);
  hGaussPymd2 = vision.Pyramid('PyramidLevel', level);
  hGaussPymd3 = vision.Pyramid('PyramidLevel', level);

  hRotate1 = vision.GeometricRotator('Angle',pi);

  hFFT2D1 = vision.FFT;
  hFFT2D2 = vision.FFT;

  hIFFFT2D = vision.IFFT;

  hConv2D = vision.Convolver('OutputSize', 'Valid');

  display('========== PRE-SETUP COMPLETE ==========');
  
  
  useDefaultTarget = false;
  [Img, numberOfTargets, target_image] = ...
      pattern_helper(useDefaultTarget);
  
  %{
  reader = vision.VideoFileReader('../starcraft-videos/CollosusVsMarines.mp4',...
    'VideoOutputDataType','uint8',...
    'ImageColorSpace','Intensity');
  Img = step(reader);
  release(reader);
    
  numberOfTargets = 1;
  target_image = imread('../starcraft-images/positives/Marines/Dirt Light/south (2).png');
  %}
  target_image = single(target_image);
  target_dim_nopyramid = size(target_image);
  target_image_gp = step(hGaussPymd1, target_image);
  target_energy = sqrt(sum(target_image_gp(:).^2));

  target_image_rot = step(hRotate1, target_image_gp);
  [rt, ct] = size(target_image_rot);
  Img = single(Img);
  Img = step(hGaussPymd2, Img);
  [ri, ci] = size(Img);
  r_mod = 2^nextpow2(rt + ri);
  c_mod = 2^nextpow2(ct + ci);
  
  size(target_image_rot)
  disp(rt);
  disp(c_mod-ct);
  target_image_p = [target_image_rot zeros(rt, c_mod - ct)];
  target_image_p = [target_image_p; zeros(r_mod-rt, c_mod)];

  target_fft = step(hFFT2D1, target_image_p);

  target_size = repmat(target_dim_nopyramid, [numberOfTargets, 1]);

  gain = 2^(level);
  Im_p = zeros(r_mod, c_mod, 'single');
  C_ones = ones(rt, ct, 'single');

  display('========== SETUP COMPLETE ==========');
  
  hFindMax = vision.LocalMaximaFinder( ...
              'Threshold', single(-1), ...
              'MaximumNumLocalMaxima', numberOfTargets, ...
              'NeighborhoodSize', floor(size(target_image_gp)/2)*2 - 1);
  
  sz = get(0, 'ScreenSize');
  pos = [20 sz(4)-400 400 300];
  hROIPattern = vision.VideoPlayer('Name', 'Overlay the ROI on the target',...
                  'Position', pos);

  hPlot = videopatternplots('setup', numberOfTargets, threshold);

  display('========== STARING VIDEO PROCESSING ==========');
  
  while ~isDone(hVideoSrc)
    Im = step(hVideoSrc);
    Im_gp = step(hGaussPymd3, Im);

    Im_p(1:ri, 1:ci) = Im_gp;
    img_fft = step(hFFT2D2, Im_p);
    corr_freq = img_fft .* target_fft;
    corrOutput_f = step(hIFFFT2D, corr_freq);
    corrOutput_f = corrOutput_f(rt:ri, ct:ci);

    IUT_energy = (Im_gp).^2;
    IUT = step(hConv2D, IUT_energy, C_ones);
    IUT = sqrt(IUT);

    norm_Corr_f = (corrOutput_f) ./ (IUT * target_energy);
    xyLocation = step(hFindMax, norm_Corr_f);

    linear_index = sub2ind([ri-rt, ci-ct]+1, xyLocation(:,2),xyLocation(:,1));

    norm_Corr_f_linear = norm_Corr_f(:);
    norm_Corr_value = norm_Corr_f_linear(linear_index);
    detect = (norm_Corr_value > threshold);
    target_roi = zeros(length(detect), 4);
    ul_corner = (gain.*(xyLocation(detect, :)-1))+1;
    target_roi(detect,:) = [ul_corner, fliplr(target_size(detect,:))];

    Imf = insertShape(Im, 'Rectangle', target_roi, 'Color', 'green');

    videopatternplots('update', hPlot, norm_Corr_value);
    step(hROIPattern, Imf);
  end

  release(hVideoSrc);

end
