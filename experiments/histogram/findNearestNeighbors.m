function [values indices] = findNearestNeighbors(distanceMatrix, k)
    % Given a distance matrix this function will return a vector of the 
    % k nearest neighbors (the min k values of each row)
    %
    % returns:
    %     values - a matrix of the actual values of the k nearest neighbors (for each row)
    %     indices - a matrix of the indices of the k nearest neighbors (for each row)

    length = size(distanceMatrix, 1);    
    values = zeros(length, k);
    indices = zeros(length, k);
    
    [maxVal maxInd] = max(distanceMatrix, [], 2);
    
    for i = 1:k
       [minVal minInd] = min(distanceMatrix, [], 2);
       values(:,i) = minVal;
       indices(:,i) = minInd;
       
       for j = 1:length
           distanceMatrix(j,minInd(j)) = maxVal(j);
       end
    end
end