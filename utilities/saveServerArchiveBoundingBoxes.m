
% saveServerArchiveBoundingBoxes
% 
% This is a function which will take a directory of images, run all of the 
% detectors upon it, and save all of the bounding boxes found in the images
% to a specified output directory

function saveServerArchiveBoundingBoxes()
    commandCenterDetector = vision.CascadeObjectDetector( 'cascade-cc-out2.xml' );
    vespeneDetector = vision.CascadeObjectDetector( 'cascade-vespene-out-sbrown3.xml' );
    barracksDetector = vision.CascadeObjectDetector( 'cascade-barracks-out.xml' );

    destDir = '../starcraft-images/server-boxes/';
    if ~exist(destDir, 'dir')
        mkdir(destDir);
    end

    testImages = getAllFiles('../starcraft-images/server-archive/')
    imgNum = 1;
    for i = 1 : length(testImages)
        img = imread( testImages{i} );
        img = removeHUD( img );

        saveImagesForDetector(img, commandCenterDetector, strcat(destDir, 'commandCenter/'));
        saveImagesForDetector(img, vespeneDetector, strcat(destDir, 'vespene/'));
        saveImagesForDetector(img, vespeneDetector, strcat(destDir, 'barracks/'));

        imgNum = imgNum + 1;
    end
end

function saveImageForDetector(img, detector, destDir, imgNum)
    if ~exist(destDir, 'dir')
        mkdir(destDir);
    end

    bbox = step(detector, img);
    saveBoundingBoxes(img, bbox, imgNum);
end
