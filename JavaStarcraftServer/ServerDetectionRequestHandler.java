import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;


/**
 * A helper class for the StarcraftServer. Whenever it receives a connection, 
 * it gets passed onto this Thread subclass, so that it can handle the request 
 * for matlab data.
 * 
 * Protocol:
 *     1) Read in comma-separated line of units of interest
 *           Write this out into the Query Unit File for Matlab
 *     2) Receive the screenshot image
 *     3) Write the Image Ready File to tell matlab that the screenshot is ready
 *     4) Wait for the Matlab Response File (but do not read it immediately 
 *          when it appears, since it might be incomplete)
 *     5) Wait for the Matlab Response Ready File
 *     6) Read in the Matlab response, and then delete it.
 *     7) Delete the Matlab Response Ready File
 *     8) Send response back to the StarCraft AI Client
 *     9) Wait for ACK. If no ACK within timeout time, retry step 8
 * 
 * @author Stephen
 *
 */
public class ServerDetectionRequestHandler implements Runnable {
    Socket socket;
    BufferedWriter os;
    boolean connectionNeverGotEstablished;

    ServerDetectionRequestHandler(Socket s){
        try{
            connectionNeverGotEstablished = false;
            socket = s;

            os = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        } 
        catch (IOException e) {
            connectionNeverGotEstablished = true;
            e.printStackTrace();
        }
    }

    public void run() {
        if(connectionNeverGotEstablished) {
            System.out.println("Connection never got established");
            return;
        }

        String screenShotKey = "";
        try{      
        	long startTime = System.nanoTime();
            BufferedInputStream reader = new BufferedInputStream(socket.getInputStream());

            // Receive Request Key and verify that this isn't a stale request
            byte[] array = new byte[20];
            reader.read(array, 0, 20);
            screenShotKey = new String(array).trim();
            
            boolean shouldRun = 
                    ServerDetectionsManager.SINGLETON.testAndSetLastJobKeyLaunched(screenShotKey);
            System.out.println(screenShotKey + ": starting...");
            if( ! shouldRun ){
                // Close socket and end connection if this is a stale request
                return;
            }
            
            // Receive the units we're interested in and write those to a file
            //String allRequestedUnits = reader.readLine();
            String allRequestedUnits = "";
            int c = 0;
            do{
            	c = reader.read();
            	allRequestedUnits += (char)c;
            }while( c != '\n');
            
            String[] units = allRequestedUnits.split(",");
            File unitQueryFile = new File(Util.QUERY_IMG_DEST_PATH + "queryUnits-" + screenShotKey + ".txt");
            PrintWriter pw = new PrintWriter(unitQueryFile);
            for(String unit : units){
                pw.println(unit);
            }
            pw.close();

            // Next, read in the actual image from the socket
            //System.out.println("Attempting to read image");
            long startImgIo = System.nanoTime();
            BufferedImage image = ImageIO.read(reader);
            double elapsed = (System.nanoTime() - startImgIo) / Math.pow(10, 9);
            System.out.println(screenShotKey + ": Img IO Time: " + elapsed);
            //System.out.println("Received Image");
            File outputFile = new File(Util.QUERY_IMG_DEST_PATH + "queryImg-" + screenShotKey + ".png");
            ImageIO.write(image, "png", outputFile);
            //System.out.println("Image written to disk");

            // Now, write file to tell Matlab that the query image is ready
            File doneWritingImageIndicator = new File(Util.QUERY_IMG_DEST_PATH + "queryImageReady-" + screenShotKey + ".txt");
            PrintWriter pw2 = new PrintWriter( doneWritingImageIndicator );
            pw2.close();

            // Now poll for a MATLAB response file - two stages
            // First wait for the response to appear, then wait for the response
            // ready to appear (The first stage is like a warning that the response is almost ready)
            File responseFile = new File(Util.QUERY_IMG_DEST_PATH + "response-" + screenShotKey + ".txt");
            //System.out.print("Waiting for Response File to exist");
            while( ! responseFile.exists() ){
                os.write("Heartbeat\n");
                os.flush();
                
                // Check if Matlab has decided to skip this job
                if (ServerDetectionsManager.SINGLETON.isStaleJob(screenShotKey)){
                    System.out.println(screenShotKey + ": Stale Job, cancelling...");
                    os.write("Cancel\n");
                    os.flush();
                    outputFile.delete();
                    doneWritingImageIndicator.delete();
                    
                    Thread.sleep(1000);
                    return;
                }
                
                Thread.sleep(200); // Sleep fifth of second
            }
            //System.out.println("Response File Exists, waiting for ready file...");

            File responseFileReady = new File(Util.QUERY_IMG_DEST_PATH + "responseReady-" + screenShotKey + ".txt");
            while( ! responseFileReady.exists() ){
                //System.out.println("Waiting for Response File to exist");
                os.write("Heartbeat\n");
                os.flush();
                Thread.sleep(50); // Speedier sleep - almost ready!
            }                

            // Finally, read the file, and then send it over the network
            Scanner scnr = new Scanner( responseFile );

            List<String> responseLines = new LinkedList<String>();
            while(scnr.hasNextLine()){
                String line = scnr.nextLine();
                System.out.println(screenShotKey + ": " + line);
                responseLines.add(line);
            }

            ServerDetectionsManager.SINGLETON.registerJobCompletion( screenShotKey );
        
            // Safe to delete the response files now
            responseFile.delete();
            responseFileReady.delete();

            if (responseLines.isEmpty()){
                System.out.println("WARNING: Response file was blank");
            }
            for(String line : responseLines){
                os.write(line + "\n");
            }
            os.write("\n");
            os.flush();
        
            os.write("STOPSTOPSTOP\n");
            os.flush();
            
            double elapsedTime = (System.nanoTime() - startTime)/Math.pow(10,9);
            System.out.println(screenShotKey + ": elapsed time: " + elapsedTime);

            // Sleep so that Windows clients don't think that we've hung up
            // on them
            Thread.sleep(1000);
        }
        catch(FileNotFoundException e){
            System.out.println("Error: File Not Found");    
        }
        catch(IllegalArgumentException e){
            System.out.println("No image available");
        }
        catch(IOException e){
            System.out.println("Error: IOException");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        catch(Exception e){
            System.out.println("Error: Exception in Submission Handler");
            e.printStackTrace();
        }
        finally{               
            //System.out.println(screenShotKey + ": Terminating");
            Util.closeSocket(socket);
        }

    }   
}
