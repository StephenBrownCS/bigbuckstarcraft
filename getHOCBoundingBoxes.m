function bbox = getHOCBoundingBoxes(img, NUM_BINS, megaHistogram, THRESHOLD)
    addpath('utilities');
    addpath('histogram-of-colors');
    
    if ~exist('NUM_BINS', 'var')
        NUM_BINS = 32;
    end   
    if ~exist('megaHistogram', 'var')
        [megaHistogram, minScore, maxScore, avgScore] = trainHistogramOfColors(NUM_BINS);
        THRESHOLD = maxScore;
    end
    WINDOW_SIZE = 72;
    STEP_SIZE = WINDOW_SIZE/3;
    bbox = [];
    for row = 1 : STEP_SIZE : size(img, 1) - WINDOW_SIZE
        for col = 1 : STEP_SIZE : size(img, 2) - WINDOW_SIZE
            newHist = computeHistogramOfColors(img(row:row+WINDOW_SIZE, col:col+WINDOW_SIZE,:),NUM_BINS);
            score = megaHistogram - uint8(newHist);
            score = score .* score;
            if sum(sum(score)) < THRESHOLD
                if isempty(bbox)
                    bbox = [col, row, WINDOW_SIZE, WINDOW_SIZE];
                else 
                    bbox = [bbox; col, row, WINDOW_SIZE, WINDOW_SIZE]; 
                end
            end
        end
    end
    
    bbox = mergeBoxes(bbox, 3);    
end