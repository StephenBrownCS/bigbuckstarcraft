function subsample( in, outPrefix, WINDOW_SIZE_HEIGHT, WINDOW_SIZE_WIDTH )
   img = imread(in); 
   WIDTH = size(img, 2);
   HEIGHT = size(img, 1);
   for i = 1:floor(WINDOW_SIZE_HEIGHT/4):HEIGHT - WINDOW_SIZE_HEIGHT
       for j = 1:floor(WINDOW_SIZE_WIDTH/4):WIDTH - WINDOW_SIZE_WIDTH
          newImg = img(i:i+WINDOW_SIZE_HEIGHT, j:j+WINDOW_SIZE_WIDTH, :); 
          path = strcat(outPrefix, '_', num2str(i * WIDTH + j - WIDTH), '.png');
          imwrite(newImg, path);
          disp(strcat('Wrote to ', path));
       end
   end
end