function [linearCodes] = findLinearCodes( instances, distanceMatrix, dictionary )
    % INPUTS: 
    %   instances - an m x n matrix where each of the m rows is a data instance (SIFT feature)
    %              with n features
    %   distanceMatrix - a m x d matrix containing the distances from each of the 
    %                  m instances to the d codewords in our dictionary
    %   dictionary - the set of codewords used for the coding scheme
    %
    % OUTPUTS:
    %   linearCodes - a m x d matrix representing the codes for each of them instances

    % Number of Nearest Neighbors
    K = 5;
    NUM_INSTANCES = size(instances, 1);
    NUM_CODEWORDS = size(dictionary, 1);
    
    % FIND NEAREST NEIGHBORS
    [kNearestNeighbors, neighborIndices] = findNearestNeighbors(distanceMatrix, K);
    
    % FIND THE LOCALLY-CONSTRAINED LINEAR CODES FOR EACH INSTANCE
    linearCodes = [];
    for i = 1 : NUM_INSTANCES
        kNearestCodewords = dictionary(neighborIndices(i, :), :);
        nearestNeighborCode = JiaWeightLoader( kNearestCodewords, instances(i, :)', K );
        
        % Map code into big vector of all dictionary codewords
        code = zeros( NUM_CODEWORDS, 1);
        code(neighborIndices(i, :), :) = nearestNeighborCode;       
        linearCodes = [linearCodes ; code'];
    end
    
end


function [codes] = JiaWeightLoader( kNearestCodewords, dataInstance, K)
    % demo code to solve Eq.7 in the LLC paper
    %
    % INPUTS:
    %         kNearestCodewords (B from paper)
    %              These are the k nearest codewords for the given data instance (SIFT feature)
    %         dataInstance:
    %               An N x 1 vector representing a single data instance with N features
    %         K:
    %               The number of nearest neighbors
    % OUTPUTS: c hats - set of codes for each training instance x
    %          codes
    %               A D x 1 vector representing the code for this instance
    %               (where D is the size of the dictionary)
    %               It will have K non-zero elements

    one = ones(K, 1);

    % compute data covariance matrix C
    B_1x = kNearestCodewords - one * dataInstance';
    C = B_1x * B_1x';

    % reconstruct LLC code
    codes = C \ one;
    codes = codes /sum(codes);
end