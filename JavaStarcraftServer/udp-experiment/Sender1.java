import java.awt.image.*;
import java.io.*;
import java.net.*;

import javax.imageio.*;

public class Sender1 {

    public static void main(String args[]) {        

        try {
            args = new String[]{"whatever", "localhost", "8003", "file.png"};

            if (args.length!= 4){
                throw new IOException("Usage: java SenderX localhost <Port> <Filename");
            }else{

                InetAddress sock;
                sock = InetAddress.getByName(args[1]);

                int DUMMY_PORT = Integer.parseInt(args[2]);

                InputStream is = new BufferedInputStream(new FileInputStream(args[3]));
                BufferedImage pic = ImageIO.read(is);
                ByteArrayOutputStream inStream = new ByteArrayOutputStream();

                //File sourceimage = new File("source.gif");
                //BufferedImage pic = ImageIO.read(sourceimage);
                ImageIO.write(pic, "PNG",inStream);
                byte[] bytes = inStream.toByteArray();
                int PACKET_LENGTH = 1024;

                DatagramSocket sendSocket = new DatagramSocket();
                byte[] sendPacket = new byte[PACKET_LENGTH+3];
                int offset = 0;

                byte flag = 0;

                long startTime = System.nanoTime();
                for (int i=0;i<bytes.length % PACKET_LENGTH;i++) { 

                    //fill the packet to be sent with the next series of bytes
                    for(int j=0;j<PACKET_LENGTH;j++){
                        if (j + offset < bytes.length){
                            sendPacket[j]=bytes[j+offset];
                        }
                        //System.out.println(sendPacket[i]);
                    }


                    offset += PACKET_LENGTH;

                    DatagramPacket dummyPacket = new DatagramPacket(bytes, PACKET_LENGTH + 3, sock, DUMMY_PORT);
                    System.out.println(flag);
                    try {
                        sendSocket.send(dummyPacket);
                        System.out.println("send dummy packet succeeded so assume already connected");

                    } catch (NoRouteToHostException nrthe) {
                        System.out.println("alreadyConnected: no route to host so assume not connected");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                double elapsedTime = System.nanoTime() - startTime;
                System.out.println(elapsedTime / Math.pow(10, 9));
                sendSocket.close();
            }
        }
        catch (IOException es) {
            es.printStackTrace();
        }   
    }

}