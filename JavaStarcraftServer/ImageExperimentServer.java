import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import javax.imageio.ImageIO;


public class ImageExperimentServer {
    public static void main(String[] args){
        ImageExperimentServer s = new ImageExperimentServer();
        s.run();
    }

    public void run() {

        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(8003);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            System.exit(0);
        }
        while(true){
            String screenShotKey = "";
            Socket socket = null;
            try{      
                socket = serverSocket.accept();

                long startTime = System.nanoTime();
                BufferedInputStream reader = new BufferedInputStream(socket.getInputStream());

                // Receive Request Key and verify that this isn't a stale request
                byte[] array = new byte[20];
                reader.read(array, 0, 20);
                screenShotKey = new String(array).trim();

                boolean shouldRun = 
                        ServerDetectionsManager.SINGLETON.testAndSetLastJobKeyLaunched(screenShotKey);
                System.out.println(screenShotKey + ": starting...");

                // Receive the units we're interested in and write those to a file
                //String allRequestedUnits = reader.readLine();
                String allRequestedUnits = "";
                int c = 0;
                do{
                    c = reader.read();
                    allRequestedUnits += (char)c;
                }while( c != '\n');

                String[] units = allRequestedUnits.split(",");
                File unitQueryFile = new File(Util.QUERY_IMG_DEST_PATH + "queryUnits-" + screenShotKey + ".txt");
                PrintWriter pw = new PrintWriter(unitQueryFile);
                for(String unit : units){
                    pw.println(unit);
                }
                pw.close();

                // Next, read in the actual image from the socket
                //System.out.println("Attempting to read image");
                long startImgIo = System.nanoTime();
                BufferedImage image = ImageIO.read(reader);
                double elapsed = (System.nanoTime() - startImgIo) / Math.pow(10, 9);
                System.out.println(screenShotKey + ": Img IO Time: " + elapsed);
                //System.out.println("Received Image");
                File outputFile = new File(Util.QUERY_IMG_DEST_PATH + "queryImg-" + screenShotKey + ".png");
                ImageIO.write(image, "png", outputFile);
                //System.out.println("Image written to disk");
            }
            catch(FileNotFoundException e){
                System.out.println("Error: File Not Found");    
            }
            catch(IllegalArgumentException e){
                System.out.println("No image available");
            }
            catch(IOException e){
                System.out.println("Error: IOException");
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            catch(Exception e){
                System.out.println("Error: Exception in Submission Handler");
                e.printStackTrace();
            }
            finally{               
                System.out.println(screenShotKey + ": Terminating");
                Util.closeSocket(socket);
            }
        }

    }   
}
