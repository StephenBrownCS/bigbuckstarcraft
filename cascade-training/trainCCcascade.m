function detector = trainCCcascade()
    addpath('../utilities');

    detectorFile = '../cascade-models/cascade-cc-out4.xml';
    rerun = input('Retrain? (y or n) ', 's') == 'y';
        
    if rerun
        delete(detectorFile);
        posImgDir = '../starcraft-images/command-center/';
        negImgDir = '../starcraft-images/new-negatives-128/';
        posFiles = getAllFiles(posImgDir);
        negFiles = getAllFiles(negImgDir);

        numPosFiles = size(posFiles, 1);
        numNegFiles = size(negFiles, 1);
        display(strcat('# of positive files: ', num2str(numPosFiles)));
        display(strcat('# of negative files: ', num2str(numNegFiles)));

        numObjectsPerImg = 1;

        positives = struct('imageFilename', posFiles, 'objectBoundingBoxes', ones(numObjectsPerImg, 4));
        % Set each bounding box to be [1 1 imgWidth imgHeight]
        % Each bounding box is in the format, [x y width height]
        for i = 1:numPosFiles
            img = imread(posFiles{i});
            positives(i).objectBoundingBoxes(1,3) = size(img,2);
            positives(i).objectBoundingBoxes(1,4) = size(img,1);
        end

        import vision.*;
        % The false alarm rate is the fraction of negative training samples 
        %     incorrectly classified as positive samples.
        trainCascadeObjectDetector(detectorFile, positives, negFiles,...
            'ObjectTrainingSize', [100 128],...
            'NegativeSamplesFactor', 2,...
            'NumCascadeStages', 14,...
            'FalseAlarmRate', 0.50,...
            'TruePositiveRate', 0.95,...
            'FeatureType', 'HOG');
    end
        
    detector = vision.CascadeObjectDetector( detectorFile );
        
    
    
    %testImages = getAllFiles('../starcraft-images/agria-valley-test/')
    testImages = getAllFiles('../starcraft-images/server-archive/')
    for i = 1 : length(testImages)
        % A "test" image
        %img = imread('../starcraft-images/test/Marine/sparse_units_many_marines.png');
        img = imread( testImages{i} );
        bbox = step(detector, img);
        detectedImg = insertObjectAnnotation(img, 'rectangle', bbox, 'Command Center');
        figure;
        imshow(detectedImg);
        %saveBoundingBoxes(img, bbox, 2000 + i, '/u/s/b/sbrown/private/starcraft-images/matlab-negatives/command-center/');
    end
    
end
