function saveBoundingBoxes(img, bbox, num, destDir)
    if ~exist('destDir', 'var')
        destDir = '/u/s/b/sbrown/private/starcraft-images/matlab-negatives/'; % don't forget the slash!
    end
    imgHeight = size(img, 1);
    imgWidth = size(img, 2);
    
    for i = 1 : size(bbox, 1)
        row = bbox(i,2);
        col = bbox(i,1);
        height = bbox(i,4);
        width = bbox(i,3);
        topBound = min( imgHeight, row + height);
        rightBound = min( imgWidth, col + width);
        
        croppedImg = img(row : row + height, col : col + width, :);
        %figure;
        %imshow(croppedImg);
        imwrite(croppedImg, strcat(destDir, 'matlab-negative-', num2str(num), '-', num2str(i), '.png'));
    end
end