function svm = hogExperiment()
    addpath('spact-matlab');
    addpath('histogram');
    addpath('liblinear-1.94/matlab');
    
    posDirName = '../starcraft-images/south-positives/';
    negDirName = '../starcraft-images/negatives/';

    SVM_FILE = 'svm-centrist-hist.mat';
    SVM_TRAIN_FEATURES = 'svm-centrist-hist-train.mat'
    
    % get file names and number of images
    posFileNames = dir(strcat(posDirName, '*.png'));
    numPosImages = length(posFileNames);

    % read first image for the purposes of figuring out our dimensions
    % this returns a 3D color map - rows, cols for each RGB channel
    %tempImg = padImg(imread(strcat(posDirName, posFileNames(1).name),'png'));
    %imgHeight = size(tempImg,1);
    %imgWidth = size(tempImg,2);

    % read the rest of the images into the fourth dimension
    
    
    cellSize = [4 4];
    
    tempImg = zeros(100, 100, 3);
    hog_4x4 = centrist(padImg(tempImg));
    hogFeatureSize = length(hog_4x4);
   %{
    
    display('Computing Features');
    %posTrainingFeatures = assembleHogFeatureVector( posDirName, cellSize, hogFeatureSize);
    posTrainingFeatures = assembleCentristFeatureVector( '../starcraft-images/positives', hogFeatureSize);
    negTrainingFeatures = assembleCentristFeatureVector( negDirName, hogFeatureSize);
    
    %marauderNegatives = assembleHogFeatureVector( '../starcraft-images/negatives/Marauder/Idle/', cellSize, hogFeatureSize);
    
    trainingFeatures = [posTrainingFeatures; negTrainingFeatures];
    
    numPosInstances = size( posTrainingFeatures, 1);
    classLabels = [ones( numPosInstances,1); ones(size(negTrainingFeatures, 1),1) .* 2];
    
    display('training SVM');
    trainInstances = hist_isect(trainingFeatures, trainingFeatures);
    model = train(classLabels, sparse(trainInstances));
    
    %svm = svmtrain(trainingFeatures, classLabels);
    save(SVM_FILE, 'model');
    save(SVM_TRAIN_FEATURES, 'trainingFeatures');
    %}
    
    load(SVM_FILE, 'model');
    load(SVM_TRAIN_FEATURES, 'trainingFeatures');
    
   
    display('testing SVM');
    testFeatures = assembleCentristFeatureVector( '../starcraft-images/Dirt Dark/', hogFeatureSize);
    testKernel = hist_isect( testFeatures, trainingFeatures );
    predictedLabels = predict(ones(size(testFeatures , 1), 1), sparse(testKernel), model)
    
    %svmclassify( svm, testFeatures)
    
    %testFeatures = assembleCentristFeatureVector( '../starcraft-images/positives/Marines/Grass Light/', hogFeatureSize, 1:48 );
    %svmclassify( svm, testFeatures)
    
    testFeatures =  assembleCentristFeatureVector( '../starcraft-images/Marauder/Moving/', hogFeatureSize );
    testKernel = hist_isect( testFeatures, trainingFeatures );
    predictedLabels = predict(ones(size(testFeatures , 1), 1), sparse(testKernel), model)
    %svmclassify( svm, testFeatures)
    
    %testFeatures =  assembleCentristFeatureVector( '../starcraft-images/test/Marine/mmmWindows', hogFeatureSize);
    %testKernel = hist_isect( testFeatures, trainingFeatures );
    %predictedLabels = predict(ones(size(testFeatures , 1), 1) .* 2, sparse(testKernel), model);
    %svmclassify( svm, testFeatures)
    
    testFeatures =  assembleCentristFeatureVector( '../starcraft-images/gorune/', hogFeatureSize);
    testKernel = hist_isect( testFeatures, trainingFeatures );
    predictedLabels = predict(ones(size(testFeatures , 1), 1) .* 2, sparse(testKernel), model)
        
    %testFeatures =  assembleHogFeatureVector( '../starcraft-images/test/Marine/mmmWindows', cellSize, hogFeatureSize );
    %labels = svmclassify( svm, testFeatures)
    %fileNames = getAllFiles( '../starcraft-images/test/Marine/mmmWindows' );
    %{
    for i = 1 : size(labels)
        if labels(i) == 1
            display(fileNames{i});
            figure;
            imshow(fileNames{i});
        end
    end
    %}
    
    display('done');
end

