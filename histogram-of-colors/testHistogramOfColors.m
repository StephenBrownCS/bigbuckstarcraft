megaHistogram = trainHistogramOfColors();
    NUM_BINS = 16;
    NUM_CHANNELS = 3;
   
testingImages = getAllFiles('../starcraft-images/mineral-negatives/custom-negatives/');

maxDifference = 0;
minDifference = 900000;
runningTotal = 0;

for i = 1 : length(testingImages)
        img = imread(testingImages{i});
        testHist = computeHistogramOfColors(img, NUM_BINS);
        difference = megaHistogram - uint8(testHist);
        difference = difference .* difference;
        difference = sum(sum(difference));
        if difference > maxDifference
            maxDifference = difference;
        end
        if difference < minDifference
            minDifference = difference;
            minImg = img;
        end
        runningTotal = runningTotal + difference;
end

minDifference
maxDifference

figure;
imshow(minImg);

runningTotal = runningTotal / length( testingImages )
