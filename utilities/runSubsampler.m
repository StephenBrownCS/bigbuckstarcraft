%subsample('../starcraft-images/new-big-negatives/background.jpg', '../starcraft-images/new-negatives-128/background_');
%subsample('../starcraft-images/new-big-negatives/Terrain_013.jpg', '../starcraft-images/new-negatives-128/Terrain_013_');
%subsample('../starcraft-images/new-big-negatives/Terrain013.png', '../starcraft-images/new-negatives-128/OtherTerrain013_');
%subsample('../starcraft-images/new-big-negatives/xFpey.jpg', '../starcraft-images/new-negatives-128/xFpey_');
%subsample('../starcraft-images/new-big-negatives/swarm-screenshot16-full.jpeg', '../starcraft-images/new-negatives-128/swarm-screenshot16-full_');

% Command Center - 100 high by 128 wide

%{
subsample('../starcraft-images/command-center-negatives/1373439668_terran_scv_003-full.jpg',...
    '../starcraft-images/new-cc-negatives-128/terran_scv_003-full_');

subsample('../starcraft-images/command-center-negatives/4ghostdro.png',...
    '../starcraft-images/new-cc-negatives-128/4ghostdro_');

subsample('../starcraft-images/command-center-negatives/C3iGk.jpg',...
    '../starcraft-images/new-cc-negatives-128/C3iGk_');

subsample('../starcraft-images/command-center-negatives/HomeBaseofMission3.jpg',...
    '../starcraft-images/new-cc-negatives-128/HomeBaseofMission3_');

subsample('../starcraft-images/command-center-negatives/sc2-20100406-1401205.jpg',...
    '../starcraft-images/new-cc-negatives-128/sc2-20100406-1401205._');


subsample('../starcraft-images/command-center-negatives/sc2-2012-04-04-18-51-33-80.png',...
    '../starcraft-images/new-cc-negatives-128/sc2-2012-04-04-18-51-33-80_');

subsample('../starcraft-images/command-center-negatives/sc2b-20100312-233305.jpg',...
    '../starcraft-images/new-cc-negatives-128/sc2b-20100312-233305_');

subsample('../starcraft-images/command-center-negatives/screen6.jpg',...
    '../starcraft-images/new-cc-negatives-128/screen6_');

subsample('../starcraft-images/command-center-negatives/starcraft-2-screenshot.jpg',...
    '../starcraft-images/new-cc-negatives-128/starcraft-2-screenshot_');

subsample('../starcraft-images/command-center-negatives/terran-base-ambush.jpg',...
    '../starcraft-images/new-cc-negatives-128/terran-base-ambush_');

subsample('../starcraft-images/command-center-negatives/terran-base.jpg',...
    '../starcraft-images/new-cc-negatives-128/terran-base');

subsample('../starcraft-images/command-center-negatives/terran_warhounds_have_splash_to_defend_against_mutalisks_01_19880.jpg',...
    '../starcraft-images/new-cc-negatives-128/terran_warhounds_have_splash_to_defend_against_mutalisks_01_19880_');

subsample('../starcraft-images/command-center-negatives/walling-in-workers-away-from-harassment1.jpg',...
    '../starcraft-images/new-cc-negatives-128/walling-in-workers-away-from-harassment1_');
%}


% MINERALS
%{
destDirectory = '../starcraft-images/cc-area-mineral-negatives-103/' % Don't forget the trailing slash!
height = 103;
width = 103;

files = getAllFiles('../starcraft-images/cc-area-mineral-big-negatives/');
for i = 1:length(files)
    subsample(files{i}, strcat(destDirectory, 'cc-area-mineral-negative-', num2str(i), '-103'), height, width);
end

subsample('../starcraft-images/new-big-negatives/background.jpg', strcat(destDirectory, 'background-103'), height, width);
subsample('../starcraft-images/new-big-negatives/Terrain_013.jpg', strcat(destDirectory, 'Terrain_013-103'), height, width);
subsample('../starcraft-images/new-big-negatives/xFpey.jpg', strcat(destDirectory, 'xFpey-103'), height, width);
subsample('../starcraft-images/new-big-negatives/swarm-screenshot16-full.jpeg', strcat(destDirectory, 'swarm-screenshot16-full-103'), height, width);
subsample('../starcraft-images/new-big-negatives/Terrain013.png', strcat(destDirectory, 'OtherTerrain013-103'), height, width);
%}
 %{
destDirectory = '../starcraft-images/vespene-negatives/' % Don't forget the trailing slash!
height = 150;
width = 150;
%}
%{
files = getAllFiles('../starcraft-images/vespene-big-negatives/');
files
for i = 1:length(files)
    subsample(files{i}, strcat(destDirectory, 'vespene-negative-', num2str(i), '-', num2str(height),'-',num2str(width)), height, width);
end

subsample('../starcraft-images/new-big-negatives/background.jpg', strcat(destDirectory, 'background-', num2str(height), '-', num2str(width)), height, width);
subsample('../starcraft-images/new-big-negatives/Terrain_013.jpg', strcat(destDirectory, 'Terrain_013-', num2str(height), '-', num2str(width)), height, width);
subsample('../starcraft-images/new-big-negatives/xFpey.jpg', strcat(destDirectory, 'xFpey-', num2str(height),'-',num2str(width)), height, width);
subsample('../starcraft-images/new-big-negatives/swarm-screenshot16-full.jpeg', strcat(destDirectory, 'swarm-screenshot16-full-', num2str(height), '-', num2str(width)), height, width);
subsample('../starcraft-images/new-big-negatives/Terrain013.png', strcat(destDirectory, 'OtherTerrain013-100', num2str(height),'-',num2str(width)), height, width);
%}

% did 150 and 100 on new-vespene also

% These "agria-valley negatives" are the ones from youtube, meant for being
% negative vespene geysers
%{
files = getAllFiles('../starcraft-images/agria-valley/agria-valley-big-vespene-negatives/');
files
for i = 1:length(files)
    subsample(files{i}, strcat(destDirectory, 'agria-valley/agria-valley-negative-', num2str(i), '-', num2str(height),'-',num2str(width)), height, width);
end
%}

% Agria Valley stuff from Corey

%{
destDirectory = '../starcraft-images/agria-valley-with-resources-128/2/' % Don't forget the trailing slash!
height = 128;
width = 128;

files = getAllFiles('../starcraft-images/corey-agria-valley');
files
for i = 63:length(files)
    subsample(files{i}, strcat(destDirectory, 'tile-', num2str(i), '-', num2str(height),'-',num2str(width)), height, width);
end
%}

%{
destDirectory = '../starcraft-images/stephen-windows/party-time/windows/supply-depot/' % Don't forget the trailing slash!
height = 100;
width = 100;

files = getAllFiles('../starcraft-images/stephen-windows/party-time/supply-depot');
files
for i = 1:length(files)
    subsample(files{i}, strcat(destDirectory, 'supply-depot-', num2str(i), '-', num2str(height),'-',num2str(width)), height, width);
end
%}

%%{

% PARTY TIME
unitName = 'scv';
height = 34;
width = 34;
destDirectory = strcat('../starcraft-images/stephen-windows/party-time/windows-', num2str(height), '/', unitName, '/') % Don't forget the trailing slash!
if ~exist(destDirectory, 'dir')
    mkdir(destDirectory);
end

files = getAllFiles(strcat('../starcraft-images/stephen-windows/party-time/', unitName, '/'));
files
for i = 1:length(files)
    subsample(files{i}, strcat(destDirectory, unitName, num2str(i), '-', num2str(height),'-',num2str(width)), height, width);
end
%%}

%{
function runSubsampler
    destDirectory = strcat('../starcraft-images/mineral-negatives/'); % Don't forget the trailing slash!
    
    files = getAllFiles('../starcraft-images/agria-valley-no-minerals/');
   % files
    for i = 1:length(files)
        dir = strcat(destDirectory, num2str(i), '/');
        mkdir(dir);
        subsample(files{i}, strcat(dir, 'tile-', num2str(i), '-', num2str(70)), 70, 70);
    end
end
%}
