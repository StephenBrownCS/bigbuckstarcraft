function K = linearKernel(x1, x2)

    % Evaluate a histogram intersection kernel, for example
    %
    %    K = hist_isect(x1, x2);
    %
    % where x1 and x2 are matrices containing input vectors, where 
    % each row represents a single vector.
    % If x1 is a matrix of size m x o and x2 is of size n x o,
    % the output K is a matrix of size m x n.

    %n = size(x2,1);
    %m = size(x1,1);
    %K = zeros(m,n);

    %for i = 1:m
    %    for j = 1:n
    %        K(i,j) = sum(x1(i, :) .* x2(j, :));
    %    end
    %end
    
    K = x1 * x2';
end


