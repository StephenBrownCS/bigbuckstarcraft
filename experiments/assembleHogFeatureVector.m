function [hogFeatures] = assembleHogFeatureVector( dirName, cellSize, hogFeatureSize, range )
    %dirName = strcat(dirName, '/');

    %fileNames = dir(strcat(dirName, '*.png'));
    fileNames = getAllFiles( dirName );
    if ~exist('range', 'var')
        range = 1:length(fileNames);
    end
    
    numImages = size(range,2);
    hogFeatures  = zeros( numImages, hogFeatureSize, 'single');
    for i = 1:numImages
       
        img = imread(fileNames{i});
        %if size(img,1) <= 48 && size(img,2) <= 48
           img = padImg(img);
        %end
        %figure, imshow(img);
        %img = padImg(imread(strcat(dirName, fileNames(range(i)).name),'png'));
        
        %figure(1), imshow(img);
        %hold on;
        %[hog_2x2, vis2x2] = extractHOGFeatures(img,'CellSize',[2 2]);
        %[hog_4x4, vis4x4] = extractHOGFeatures(img,'CellSize',[4 4]);
        %[hog_8x8, vis8x8] = extractHOGFeatures(img,'CellSize',[8 8]);
        %plot(vis2x2);
        %if size(img,1) <= 48 && size(img,2) <= 48
            [hog, vis] = extractHOGFeatures(img,'CellSize',cellSize);
            hogFeatures(i,:) = hog;
        %else
          %  hogFeatures(i,:) = hogFeatures(i-1,:);
        %end
    end
end

